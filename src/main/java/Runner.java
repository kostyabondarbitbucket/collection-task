import beans.People;
import collection.ImplementDeque;
import factory.PeopleFactory;
import interfaces.PeopleDao;

import java.util.Locale;
import java.util.ResourceBundle;

public class Runner {
    public static void main(String[] args) {
        ResourceBundle bundle = ResourceBundle.getBundle("in", Locale.ENGLISH);
        PeopleDao dao = PeopleFactory.getClassFromFactory(bundle);
        ImplementDeque<People> peoples = dao.getPeoples();
        peoples.removeFirst();
        peoples.printCollection();
    }
}
