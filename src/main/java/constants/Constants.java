package constants;

public class Constants {
    public static final String CSV_PATH = "people.csv";
    public static final String SOURCE = "people.source";
    public static final String CSV_DELIMITER = ";";
    public static final int FIRST_NAME_PARAM = 0;
    public static final int LAST_NAME_PARAM = 1;
    public static final int AGE_PARAM = 2;
}
