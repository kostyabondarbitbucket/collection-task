package interfaces;

import beans.People;
import collection.ImplementDeque;

public interface PeopleDao {
    ImplementDeque<People> getPeoples();
}
