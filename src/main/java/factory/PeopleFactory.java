package factory;

import constants.Constants;
import exceptions.InitException;
import impl.PeopleImplCsv;
import interfaces.PeopleDao;

import java.util.ResourceBundle;

import static constants.Constants.CSV_PATH;

public class PeopleFactory {

    private enum Source {
        CSV {
            @Override
            PeopleDao getImpl(ResourceBundle bundle) throws InitException {
                String csvPath = bundle.getString(CSV_PATH);
                return new PeopleImplCsv(csvPath);
            }
        };

        abstract PeopleDao getImpl(ResourceBundle bundle) throws InitException;
    }

    public static PeopleDao getClassFromFactory(ResourceBundle bundle) throws InitException {
        String src = bundle.getString(Constants.SOURCE);
        Source source = Source.valueOf(src.toUpperCase());
        return source.getImpl(bundle);
    }
}
