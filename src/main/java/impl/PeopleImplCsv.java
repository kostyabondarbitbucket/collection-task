package impl;

import beans.People;
import collection.ImplementDeque;
import constants.Constants;
import interfaces.PeopleDao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class PeopleImplCsv implements PeopleDao {

    private String csvFile;

    public PeopleImplCsv(String csvFile) {
        this.csvFile = csvFile;
    }

    @Override
    public ImplementDeque<People> getPeoples() {
        ImplementDeque<People> peoples = new ImplementDeque<>();
        Scanner scanner = null;
        try {
            scanner = new Scanner(new FileReader(csvFile));
        } catch (FileNotFoundException e) {
            System.err.println(" file " + csvFile + " not found");
            System.exit(0);
        }
        while (scanner.hasNext()) {
            String[] csvLine = scanner.nextLine().split(Constants.CSV_DELIMITER);
            String firstName = csvLine[Constants.FIRST_NAME_PARAM];
            String lastName = csvLine[Constants.LAST_NAME_PARAM];
            int age = Integer.parseInt(csvLine[Constants.AGE_PARAM]);


            peoples.addFirst(new People(firstName, lastName, age));
        }
        return peoples;
    }
}
