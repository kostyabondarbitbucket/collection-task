package exceptions;

import java.util.MissingResourceException;

public class InitException extends MissingResourceException {
    /**
     * Constructs a MissingResourceException with the specified information.
     * A detail message is a String that describes this particular exception.
     *
     * @param s         the detail message
     * @param className the name of the resource class
     * @param key       the key for the missing resource.
     */
    public InitException(String s, String className, String key) {
        super(s, className, key);
    }
}
